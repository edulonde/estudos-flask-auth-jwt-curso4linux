import flask

from dotenv import load_dotenv

from api.auth import blueprint as auth
from api.profile import blueprint as profile
from frontend.views import blueprint as views

load_dotenv()

app = flask.Flask(__name__)
app.register_blueprint(auth)
app.register_blueprint(profile)
app.register_blueprint(views)


@app.route("/")
def index():
    return flask.render_template('index.html')


@app.route("/login")
def login():
    return flask.render_template('login.html')


@app.route("/profile")
def profile():
    return flask.render_template('profile.html')
